const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BulletinBoard = new Schema({
    title: {
        type: String
    },
    body: {
        type: String
    },
    isPrivate: {
        type: Boolean
    }
},{
        collection: 'bulletin'
    });

module.exports = mongoose.model('BulletinBoard', BulletinBoard);