const express = require('express');
const app = express();
const businessRoutes = express.Router();

let BulletinBoard = require('../models/BulletinBoard');

businessRoutes.route('/').get(function (req, res) {
    BulletinBoard.find(function (err, bulletins){
        if(err){
            console.log(err);
        }else{
            res.json(bulletins);
        }
    });
});

businessRoutes.route('/create').post(function (req, res) {
   let bulletin = new BulletinBoard(req.body);
   bulletin.save().then(bulletin => {
    res.status(200).json({'bulletin': 'bulletin in added successfully'});
   })
});

module.exports = businessRoutes;