const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    config = require('./DB');

    const bulettinRoutes = require('./routes/bulletinBoard.route');
    const userRoutes = require('./routes/user.route');

    mongoose.Promise = global.Promise;
    mongoose.connect(config.DB, { useNewUrlParser: true }).then(
      () => {console.log('Database is connected') },
      err => { console.log('Can not connect to the database'+ err)}
    );

    var version=process.env.version || "1.0"

    const app = express();
    app.use(bodyParser.json());
    app.use(cors());
    

    app.use('/bulletins', bulettinRoutes);
    app.use('/accounts', userRoutes);

    const port = process.env.PORT || 4000;

    const server = app.listen(port, function(){
     console.log('Listening on port ' + port);
    });